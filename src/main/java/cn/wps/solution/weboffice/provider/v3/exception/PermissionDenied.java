package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.FORBIDDEN)
public class PermissionDenied extends ProviderException {
    private final int code = 40003;

    public PermissionDenied() {
        this("PermissionDenied");
    }

    public PermissionDenied(String message) {
        super(message);
    }
}
