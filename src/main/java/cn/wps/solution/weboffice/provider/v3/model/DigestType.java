package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DigestType {
    NONE(""),
    MD5("md5"),
    SHA1("sha1"),
    SHA256("sha256");

    @JsonValue
    private String jsonValue;

    DigestType(String jsonValue) {
        this.jsonValue = jsonValue;
    }
}
