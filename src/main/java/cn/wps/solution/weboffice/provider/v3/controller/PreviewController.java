package cn.wps.solution.weboffice.provider.v3.controller;

import cn.wps.solution.weboffice.provider.v3.model.DownloadInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.ProviderResponseEntity;
import cn.wps.solution.weboffice.provider.v3.model.UserPermission;
import cn.wps.solution.weboffice.provider.v3.service.PreviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PreviewController extends ProviderBaseController {

    @Autowired
    private PreviewService previewService;

    @RequestMapping(value = "/v3/3rd/files/{file_id}", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<FileInfo> fetchFile(@PathVariable("file_id") final String fileId) {
        return ProviderResponseEntity.ok(this.previewService.fetchFileInfo(fileId));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/download", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<DownloadInfo> fetchDownloadInfo(@PathVariable("file_id") final String fileId) {
        return ProviderResponseEntity.ok(this.previewService.fetchDownloadInfo(fileId));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/permission", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<UserPermission> fetchUserPermission(@PathVariable("file_id") final String fileId) {
        return ProviderResponseEntity.ok(this.previewService.fetchUserPermission(fileId));
    }
}
