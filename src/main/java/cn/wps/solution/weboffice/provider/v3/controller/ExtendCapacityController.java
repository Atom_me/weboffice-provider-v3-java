package cn.wps.solution.weboffice.provider.v3.controller;

import cn.wps.solution.weboffice.provider.v3.exception.InvalidArgument;
import cn.wps.solution.weboffice.provider.v3.exception.NotImplementException;
import cn.wps.solution.weboffice.provider.v3.model.DownloadInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileRenameRequest;
import cn.wps.solution.weboffice.provider.v3.model.ProviderResponseEntity;
import cn.wps.solution.weboffice.provider.v3.model.Watermark;
import cn.wps.solution.weboffice.provider.v3.service.ExtendCapacityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@RestController
public class ExtendCapacityController extends ProviderBaseController {
    @Autowired(required = false)
    private ExtendCapacityService service;

    private ExtendCapacityService getServiceOrThrow() {
        if (Objects.isNull(this.service)) {
            throw new NotImplementException(getRequestPath());
        }
        return this.service;
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/name", method = {RequestMethod.PUT})
    @ProviderJsonApi
    public ProviderResponseEntity<Map<String, String>> fileRename(@PathVariable("file_id") String fileId,
                                                                  @RequestBody FileRenameRequest request) {
        final String name = request.getName();
        if (name == null || name.trim().equals("")) {
            throw new InvalidArgument("new filename is empty");
        }
        this.getServiceOrThrow().renameFile(fileId, name);
        return ProviderResponseEntity.ok(Collections.emptyMap());
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/versions", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<List<FileInfo>> fileVersions(@PathVariable("file_id") String fileId,
                                                               @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                                               @RequestParam(value = "limit", required = false, defaultValue = "100") int limit) {
        return ProviderResponseEntity.ok(this.getServiceOrThrow().fileVersions(fileId, offset, limit));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/versions/{version}", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<FileInfo> fileVersion(@PathVariable("file_id") String fileId,
                                                        @PathVariable("version") int version) {
        return ProviderResponseEntity.ok(this.getServiceOrThrow().fileVersion(fileId, version));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/versions/{version}/download", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<DownloadInfo> fileVersionDownload(@PathVariable("file_id") String fileId,
                                                                    @PathVariable("version") int version) {
        return ProviderResponseEntity.ok(this.getServiceOrThrow().fileVersionDownload(fileId, version));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/watermark", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<Watermark> fileWatermark(@PathVariable("file_id") String fileId) {
        return ProviderResponseEntity.ok(this.getServiceOrThrow().fileWatermark(fileId));
    }
}
