package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalError extends ProviderException {
    private final int code = 50001;

    public InternalError() {
        this("InternalError");
    }

    public InternalError(String message) {
        super(message);
    }
}
