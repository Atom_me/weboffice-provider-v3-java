package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidArgument extends ProviderException {
    private final int code = 40005;

    public InvalidArgument() {
        this("PermissionDenied");
    }

    public InvalidArgument(String message) {
        super(message);
    }
}
