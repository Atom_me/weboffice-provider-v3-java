package cn.wps.solution.weboffice.provider.v3.exception;


import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.FORBIDDEN)
public class FileUploadNotComplete extends ProviderException {
    private final int code = 41001;

    public FileUploadNotComplete() {
        this("FileUploadNotComplete");
    }

    public FileUploadNotComplete(String message) {
        super(message);
    }
}
