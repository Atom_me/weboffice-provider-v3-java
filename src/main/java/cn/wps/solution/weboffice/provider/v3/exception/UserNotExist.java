package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserNotExist extends ProviderException {
    private final int code = 40010;

    public UserNotExist() {
        this("UserNotExist");
    }

    public UserNotExist(String message) {
        super(message);
    }
}
