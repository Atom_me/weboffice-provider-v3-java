package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FileVersionNotExist extends ProviderException {
    private final int code = 40009;

    public FileVersionNotExist() {
        this("FileVersionNotExist");
    }

    public FileVersionNotExist(String message) {
        super(message);
    }
}
