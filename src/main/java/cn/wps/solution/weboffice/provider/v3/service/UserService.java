package cn.wps.solution.weboffice.provider.v3.service;

import cn.wps.solution.weboffice.provider.v3.model.UserInfo;

import java.util.List;

public interface UserService {
    List<UserInfo> fetchUsers(List<String> userIds);
}
