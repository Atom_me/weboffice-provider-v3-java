package cn.wps.solution.weboffice.provider.v3.controller;

import cn.wps.solution.weboffice.provider.v3.exception.NotImplementException;
import cn.wps.solution.weboffice.provider.v3.model.ProviderResponseEntity;
import cn.wps.solution.weboffice.provider.v3.model.UserInfo;
import cn.wps.solution.weboffice.provider.v3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
public class UserController extends ProviderBaseController {

    @Autowired(required = false)
    private UserService userService;

    private UserService getUserServiceOrThrow() {
        if (Objects.isNull(this.userService)) {
            throw new NotImplementException(String.format("request path %s not implement", getRequestPath()));
        }
        return this.userService;
    }

    @RequestMapping(value = "/v3/3rd/users", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<List<UserInfo>> fetchUsers(@RequestParam("user_ids") List<String> userIds) {
        return ProviderResponseEntity.ok(this.getUserServiceOrThrow().fetchUsers(userIds));
    }
}
