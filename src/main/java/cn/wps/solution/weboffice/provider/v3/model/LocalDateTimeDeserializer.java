package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

class LocalDateTimeDeserializer extends StdDeserializer {
    private static final ZoneOffset OFFSET = ZoneId.systemDefault().getRules().getOffset(Instant.EPOCH);

    public LocalDateTimeDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public Object deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {
        final long seconds = parser.getLongValue();
        return LocalDateTime.ofEpochSecond(seconds, 0, OFFSET);
    }
}
