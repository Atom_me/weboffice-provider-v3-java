package cn.wps.solution.weboffice.provider.v3.service;

import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileUploadSinglePhase;

public interface SinglePhaseFileStorageService {
    FileInfo uploadFile(FileUploadSinglePhase.Request request);
}

