package cn.wps.solution.weboffice.provider.v3.controller;

import cn.wps.solution.weboffice.provider.v3.exception.NotImplementException;
import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileUploadMultiPhase;
import cn.wps.solution.weboffice.provider.v3.model.FileUploadSinglePhase;
import cn.wps.solution.weboffice.provider.v3.model.ProviderResponseEntity;
import cn.wps.solution.weboffice.provider.v3.service.MultiPhaseFileStorageService;
import cn.wps.solution.weboffice.provider.v3.service.SinglePhaseFileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@RestController
public class FileStorageController extends ProviderBaseController {

    @Autowired(required = false)
    private MultiPhaseFileStorageService multiPhase;

    @Autowired(required = false)
    private SinglePhaseFileStorageService singlePhase;

    private MultiPhaseFileStorageService getMultiPhaseServiceOrThrow() {
        if (Objects.isNull(this.multiPhase)) {
            throw new NotImplementException(String.format("request path %s not implement", getRequestPath()));
        }
        return this.multiPhase;
    }

    private SinglePhaseFileStorageService getSinglePhaseServiceOrThrow() {
        if (Objects.isNull(this.singlePhase)) {
            throw new NotImplementException(String.format("request path %s not implement", getRequestPath()));
        }
        return this.singlePhase;
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/upload/prepare", method = {RequestMethod.GET})
    @ProviderJsonApi
    public ProviderResponseEntity<Map<String, Object>> uploadPrepare(@PathVariable("file_id") String fileId) {
        return ProviderResponseEntity.ok(Collections
                .singletonMap("digest_types", this.getMultiPhaseServiceOrThrow().uploadPrepare(fileId)));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/upload/address", method = {RequestMethod.POST})
    @ProviderJsonApi
    public ProviderResponseEntity<FileUploadMultiPhase.FileUploadAddress.Response> uploadAddress(@PathVariable("file_id") String fileId,
                                                                                                 @RequestBody FileUploadMultiPhase.FileUploadAddress.Request request) {
        request.setFileId(fileId);
        return ProviderResponseEntity.ok(this.getMultiPhaseServiceOrThrow().uploadAddress(request));
    }

    @RequestMapping(value = "/v3/3rd/files/{file_id}/upload/complete", method = {RequestMethod.POST})
    @ProviderJsonApi
    public ProviderResponseEntity<FileInfo> uploadComplete(@PathVariable("file_id") String fileId,
                                                           @RequestBody FileUploadMultiPhase.FileUploadComplete.Request request) {
        request.setFileId(fileId);
        return ProviderResponseEntity.ok(this.getMultiPhaseServiceOrThrow().uploadComplete(request));
    }


    @RequestMapping(value = "/v3/3rd/files/{file_id}/upload", method = {RequestMethod.POST}, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ProviderJsonApi
    public ProviderResponseEntity<FileInfo> uploadFile(@PathVariable("file_id") String fileId,
                                                       @ModelAttribute FileUploadSinglePhase.Request request) {
        request.setFileId(fileId);
        return ProviderResponseEntity.ok(this.getSinglePhaseServiceOrThrow().uploadFile(request));
    }
}
