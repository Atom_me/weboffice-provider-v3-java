package cn.wps.solution.weboffice.provider.v3.service;

import cn.wps.solution.weboffice.provider.v3.model.DigestType;
import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileUploadMultiPhase;

import java.util.List;

// your need to implement all these methods
public interface MultiPhaseFileStorageService {
    List<DigestType> uploadPrepare(String fileId);

    FileUploadMultiPhase.FileUploadAddress.Response uploadAddress(FileUploadMultiPhase.FileUploadAddress.Request request);

    FileInfo uploadComplete(FileUploadMultiPhase.FileUploadComplete.Request request);
}


