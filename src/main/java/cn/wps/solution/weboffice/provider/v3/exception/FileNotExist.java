package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FileNotExist extends ProviderException {
    private final int code = 40004;

    public FileNotExist() {
        this("FileNotExist");
    }

    public FileNotExist(String message) {
        super(message);
    }
}
