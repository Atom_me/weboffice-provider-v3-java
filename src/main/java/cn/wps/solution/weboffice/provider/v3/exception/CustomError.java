package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.FORBIDDEN)
public class CustomError extends ProviderException{
    private final int code = 40007;

    public CustomError() {
        this("CustomError");
    }

    public CustomError(String message) {
        super(message);
    }
}
