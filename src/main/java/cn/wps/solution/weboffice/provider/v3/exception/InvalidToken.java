package cn.wps.solution.weboffice.provider.v3.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidToken extends ProviderException {
    private final int code = 40002;

    public InvalidToken() {
        this("InvalidToken");
    }

    public InvalidToken(String message) {
        super(message);
    }
}
