package cn.wps.solution.weboffice.provider.v3.service;

import cn.wps.solution.weboffice.provider.v3.model.DownloadInfo;
import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import cn.wps.solution.weboffice.provider.v3.model.UserPermission;

public interface PreviewService {
    FileInfo fetchFileInfo(String fileId);
    DownloadInfo fetchDownloadInfo(String fileId);

    UserPermission fetchUserPermission(String fileId);
}
