package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;


public class FileInfoTests {
    @Test
    @SneakyThrows
    public void testFileMarshalAndUnmarshal() {
        final ZoneOffset offset = ZoneId.systemDefault().getRules().getOffset(Instant.EPOCH);

        // no nano in now
        final LocalDateTime now = LocalDateTime
                .ofEpochSecond(LocalDateTime.now().toEpochSecond(offset), 0, offset);

        final FileInfo one = FileInfo.builder()
                .id("file_id")
                .name("file_name")
                .version(1)
                .size(1024L)
                .createTime(now)
                .modifyTime(now)
                .creatorId("creator_id")
                .modifierId("modifier_id")
                .build();

        final String json = new ObjectMapper().writeValueAsString(one);

        assertThat(json, containsString("file_id"));
        assertThat(json, containsString(String.valueOf(now.toEpochSecond(offset))));

        System.out.println(json);

        final FileInfo other = new ObjectMapper().readerFor(FileInfo.class).readValue(json);

        assertThat(one, equalTo(other));
    }
}
