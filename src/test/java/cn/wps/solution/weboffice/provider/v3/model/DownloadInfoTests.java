package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class DownloadInfoTests {

    @Test
    @SneakyThrows
    public void testDownloadInfoMarshalAndUnmarshal() {
       final Map<String, String> headers = Collections.singletonMap("foo", "bar");

        final DownloadInfo one = DownloadInfo.builder()
                .url("https://foo.bar/baz")
                .digestType(DigestType.MD5)
                .digestValue("foo_bar_baz")
                .headers(headers)
                .build();

        final String json = new ObjectMapper().writeValueAsString(one);

        System.out.println(json);

        assertThat(json, containsString("https://foo.bar/baz"));
        assertThat(json, containsString("foo_bar_baz"));

        final DownloadInfo other = new ObjectMapper().readerFor(DownloadInfo.class).readValue(json);

        assertThat(one, equalTo(other));
    }
}
