package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;

public class WatermarkTests {
    @Test
    @SneakyThrows
    public void testWatermarkMarshalAndUnmarshal() {
        final Watermark one = Watermark.builder()
                .type(Watermark.Type.TEXT)
                .value("foo")
                .font("bold 20px Serif")
                .fillStyle(Watermark.FillStyle.builder().red(192).green(192).blue(192).alpha(0.6).build())
                .build();

        final String json = new ObjectMapper().writeValueAsString(one);
        System.out.println(json);
    }
}
