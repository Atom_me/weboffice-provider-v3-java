package cn.wps.solution.weboffice.provider.v3.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class UserPermissionTests {
    @Test
    @SneakyThrows
    public void testUserPermissionMarshalAndUnmarshal() {
        final UserPermission one = UserPermission.builder()
                .userId("foo")
                .read(true)
                .comment(true)
                .build();

        final String json = new ObjectMapper().writeValueAsString(one);
        System.out.println(json);

        assertThat(json, containsString("foo"));

        final UserPermission other = new ObjectMapper().readerFor(UserPermission.class).readValue(json);

        assertThat(one, equalTo(other));
        assert other.isRead();
        assert other.isComment();
        assert !other.isUpdate();
    }
}
